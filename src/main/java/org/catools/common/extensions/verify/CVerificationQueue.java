package org.catools.common.extensions.verify;

import org.slf4j.Logger;

/**
 * Build a sequence of verifications using method from different verification classes
 */
public interface CVerificationQueue<T extends CVerificationBuilder> {

    Logger getLogger();

    T queue(CVerificationInfo verificationInfo);
}
