package org.catools.common.extensions.verify;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;

@Slf4j
public class CBaseVerification<T extends CVerificationQueue> {
    protected final Logger logger;
    protected T verifier;

    protected CBaseVerification(T verifier) {
        this.verifier = verifier;
        this.logger = log;
    }

    protected CBaseVerification(T verifier, Logger logger) {
        this.verifier = verifier;
        this.logger = logger;
    }

    protected T queue(CVerificationInfo verificationInfo) {
        return (T) verifier.queue(verificationInfo);
    }
}
