package org.catools.common.extensions.verify;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;

import java.math.BigDecimal;

/**
 * Build a sequence of verifications using method from different verification classes
 *
 * @param <T> represent any classes which extent {@link CVerificationBuilder}.
 * @see CBooleanVerification
 * @see CCollectionVerification
 * @see CDateVerification
 * @see CFileVerification
 * @see CNumberVerification
 * @see CObjectVerification
 * @see CStringVerification
 */
@Slf4j
public abstract class CVerificationBuilder<T extends CVerificationBuilder> implements CVerificationQueue<T> {
    public final CObjectVerification<T> Object;
    public final CCollectionVerification<T> Collection;
    public final CMapVerification<T> Map;
    public final CBooleanVerification<T> Bool;
    public final CDateVerification<T> Date;
    public final CStringVerification<T> String;
    public final CFileVerification<T> File;
    public final CNumberVerification<T, Long> Long;
    public final CNumberVerification<T, BigDecimal> BigDecimal;
    public final CNumberVerification<T, Double> Double;
    public final CNumberVerification<T, Float> Float;
    public final CNumberVerification<T, Integer> Int;
    public final Logger logger;

    public CVerificationBuilder() {
        this(log);
    }

    public CVerificationBuilder(Logger logger) {
        this.logger = logger;
        this.Object = new CObjectVerification(this, logger);
        this.Collection = new CCollectionVerification(this, logger);
        this.Map = new CMapVerification(this, logger);
        this.Bool = new CBooleanVerification(this, logger);
        this.Date = new CDateVerification(this, logger);
        this.String = new CStringVerification(this, logger);
        this.File = new CFileVerification(this, logger);

        this.BigDecimal = new CNumberVerification(this, logger);
        this.Double = new CNumberVerification(this, logger);
        this.Float = new CNumberVerification(this, logger);
        this.Long = new CNumberVerification(this, logger);
        this.Int = new CNumberVerification(this, logger);
    }

    @Override
    public Logger getLogger() {
        return logger;
    }

    public abstract T queue(CVerificationInfo verificationInfo);
}
