package org.catools.common.extensions.verify.interfaces;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.catools.common.extensions.verify.CVerificationInfo;
import org.catools.common.extensions.verify.CVerificationQueue;
import org.catools.common.extensions.wait.interfaces.CBaseWaiter;
import org.catools.common.utils.CStringUtil;

import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * <p>CBaseVerifier is an interface to hold shared method between all verifier classes.</p>
 */
//
public interface CBaseVerifier<O> extends CBaseWaiter<O> {

    default <A, B, V extends CVerificationQueue> void _verify(V verificationQueue,
                                                              Function<O, A> actualProvider,
                                                              Supplier<B> expectedSupplier,
                                                              boolean printDiff,
                                                              BiFunction<A, B, Boolean> verifyMethod,
                                                              final String message,
                                                              final Object... params) {
        if (_useWaiter()) {
            verificationQueue.queue(new CVerificationInfo<>(() -> actualProvider.apply(get()),
                    expectedSupplier,
                    CStringUtil.format(message, params),
                    printDiff,
                    getDefaultWaitInSeconds(),
                    getDefaultWaitIntervalInMilliSeconds(),
                    verifyMethod));
        } else {
            verificationQueue.queue(new CVerificationInfo<>(() -> actualProvider.apply(get()),
                    expectedSupplier,
                    CStringUtil.format(message, params),
                    printDiff,
                    verifyMethod));
        }
    }

    default <A, B, V extends CVerificationQueue> void _verify(V verificationQueue,
                                                              A actual,
                                                              B expected,
                                                              boolean printDiff,
                                                              BiFunction<A, B, Boolean> verifyMethod,
                                                              final String message,
                                                              final Object... params) {
        if (_useWaiter()) {
            verificationQueue.queue(new CVerificationInfo<>(() -> actual,
                    () -> expected,
                    CStringUtil.format(message, params),
                    printDiff,
                    getDefaultWaitInSeconds(),
                    getDefaultWaitIntervalInMilliSeconds(),
                    verifyMethod));
        } else {
            verificationQueue.queue(new CVerificationInfo<>(() -> actual,
                    () -> expected,
                    CStringUtil.format(message, params),
                    printDiff,
                    verifyMethod));
        }
    }

    default <A, B, V extends CVerificationQueue> void _verify(V verificationQueue,
                                                              A actual,
                                                              B expected,
                                                              boolean printDiff,
                                                              BiFunction<A, B, Boolean> verifyMethod,
                                                              final int waitInSeconds,
                                                              final int intervalInMilliSeconds,
                                                              final String message,
                                                              final Object... params) {
        verificationQueue.queue(new CVerificationInfo<>(() -> actual,
                () -> expected,
                CStringUtil.format(message, params),
                printDiff,
                waitInSeconds,
                intervalInMilliSeconds,
                verifyMethod));
    }

    default <A, B, V extends CVerificationQueue> void _verify(V verificationQueue,
                                                              B expected,
                                                              boolean printDiff,
                                                              BiFunction<A, B, Boolean> verifyMethod,
                                                              final String message,
                                                              final Object... params) {
        if (_useWaiter()) {
            verificationQueue.queue(new CVerificationInfo<>(() -> (A) get(),
                    () -> expected,
                    CStringUtil.format(message, params),
                    printDiff,
                    getDefaultWaitInSeconds(),
                    getDefaultWaitIntervalInMilliSeconds(),
                    verifyMethod));
        } else {
            verificationQueue.queue(new CVerificationInfo<>(() -> (A) get(),
                    () -> expected,
                    CStringUtil.format(message, params),
                    printDiff,
                    verifyMethod));
        }
    }

    default <A, B, V extends CVerificationQueue> void _verify(V verificationQueue,
                                                              B expected,
                                                              boolean printDiff,
                                                              BiFunction<A, B, Boolean> verifyMethod,
                                                              BiConsumer<A, B> onFail,
                                                              final String message,
                                                              final Object... params) {
        if (_useWaiter()) {
            verificationQueue.queue(new CVerificationInfo<>(() -> (A) get(),
                    () -> expected,
                    CStringUtil.format(message, params),
                    printDiff,
                    getDefaultWaitInSeconds(),
                    getDefaultWaitIntervalInMilliSeconds(),
                    verifyMethod,
                    onFail));
        } else {
            verificationQueue.queue(new CVerificationInfo<>(() -> (A) get(),
                    () -> expected,
                    CStringUtil.format(message, params),
                    printDiff,
                    verifyMethod,
                    onFail));
        }
    }

    default <A, B, V extends CVerificationQueue> void _verify(V verificationQueue,
                                                              B expected,
                                                              boolean printDiff,
                                                              BiFunction<A, B, Boolean> verifyMethod,
                                                              final int waitInSeconds,
                                                              final String message,
                                                              final Object... params) {
        _verify(verificationQueue, expected, printDiff, verifyMethod, waitInSeconds, getDefaultWaitIntervalInMilliSeconds(), message, params);
    }

    default <A, B, V extends CVerificationQueue> void _verify(V verificationQueue,
                                                              B expected,
                                                              boolean printDiff,
                                                              BiFunction<A, B, Boolean> verifyMethod,
                                                              BiConsumer<A, B> onFail,
                                                              final int waitInSeconds,
                                                              final String message,
                                                              final Object... params) {
        _verify(verificationQueue, expected, printDiff, verifyMethod, onFail, waitInSeconds, getDefaultWaitIntervalInMilliSeconds(), message, params);
    }

    default <A, B, V extends CVerificationQueue> void _verify(V verificationQueue,
                                                              B expected,
                                                              boolean printDiff,
                                                              BiFunction<A, B, Boolean> verifyMethod,
                                                              final int waitInSeconds,
                                                              final int intervalInMilliSeconds,
                                                              final String message,
                                                              final Object... params) {
        verificationQueue.queue(new CVerificationInfo<>(() -> (A) get(),
                () -> expected,
                CStringUtil.format(message, params),
                printDiff,
                waitInSeconds,
                intervalInMilliSeconds,
                verifyMethod));
    }

    default <A, B, V extends CVerificationQueue> void _verify(V verificationQueue,
                                                              B expected,
                                                              boolean printDiff,
                                                              BiFunction<A, B, Boolean> verifyMethod,
                                                              BiConsumer<A, B> onFail,
                                                              final int waitInSeconds,
                                                              final int intervalInMilliSeconds,
                                                              final String message,
                                                              final Object... params) {
        verificationQueue.queue(new CVerificationInfo<>(() -> (A) get(),
                () -> expected,
                CStringUtil.format(message, params),
                printDiff,
                waitInSeconds,
                intervalInMilliSeconds,
                verifyMethod,
                onFail));
    }

    default String getDefaultMessage(final String methodDescription, final Object... params) {
        return getDefaultMessage(String.format(methodDescription, params));
    }

    default String getDefaultMessage(final String methodDescription) {
        if (CStringUtil.isBlank(getVerifyMessagePrefix())) {
            return "Verify " + methodDescription + ".";
        }
        return String.format("Verify %s %s.", getVerifyMessagePrefix(), methodDescription);
    }

    boolean _useWaiter();

    @JsonIgnore
    default String getVerifyMessagePrefix() {
        return "";
    }
}
