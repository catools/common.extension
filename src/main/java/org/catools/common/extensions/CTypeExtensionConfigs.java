package org.catools.common.extensions;

import org.catools.common.utils.CConfigUtil;

public class CTypeExtensionConfigs {
    public static int getDefaultWaitInSeconds() {
        return CConfigUtil.getIntegerOrElse(Configs.EXTENSION_DEFAULT_WAIT_IN_SECONDS.name(), 5);
    }

    public static int getDefaultWaitIntervalInMilliSeconds() {
        return CConfigUtil.getIntegerOrElse(Configs.EXTENSION_DEFAULT_WAIT_INTERVAL_IN_MILLIS.name(), 10);
    }

    private enum Configs {
        EXTENSION_DEFAULT_WAIT_INTERVAL_IN_MILLIS, EXTENSION_DEFAULT_WAIT_IN_SECONDS
    }
}
