package org.catools.common.extensions.types;

import org.catools.common.extensions.states.interfaces.CBooleanState;
import org.catools.common.extensions.verify.interfaces.CBooleanVerifier;
import org.catools.common.extensions.wait.interfaces.CBooleanWaiter;

/**
 * <p>CStaticBooleanExtension is an central interface where we extend all boolean related interfaces so adding new functionality will be much easier.</p>
 */
public abstract class CStaticBooleanExtension implements CBooleanWaiter, CBooleanVerifier, CBooleanState {

    @Override
    public boolean _useWaiter() {
        return false;
    }

    @Override
    public int hashCode() {
        return get().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return isEqual((Boolean) obj);
    }

    @Override
    public String toString() {
        return get().toString();
    }
}
