package org.catools.common.extensions.types;

import org.catools.common.extensions.waitVerify.interfaces.CBooleanWaitVerifier;

/**
 * <p>CDynamicBooleanExtension is an central interface where we extend all boolean related interfaces so adding new functionality will be much easier.</p>
 */
public abstract class CDynamicBooleanExtension extends CStaticBooleanExtension implements CBooleanWaitVerifier {

    @Override
    public boolean _useWaiter() {
        return true;
    }

}
