package org.catools.common.extensions.types.interfaces;

import org.catools.common.extensions.states.interfaces.CIterableState;
import org.catools.common.extensions.verify.interfaces.CIterableVerifier;
import org.catools.common.extensions.wait.interfaces.CIterableWaiter;

/**
 * <p>CStaticIterableExtension is an central interface where we extend all Iterable related interfaces so adding new functionality will be much easier.</p>
 * <strong>
 * <p>Java does not allow to override Object methods in interface level so this is something we should care about it manually.</p>
 * <p>Make sure to override equals, hashCode (and if needed toString methods) in your implementations.</p>
 * <code>
 *
 * @Override public int hashCode() {
 * return getValue().hashCode();
 * }
 * @Override public boolean equals(Object obj) {
 * return isEqual(obj);
 * }
 * </code>
 * </strong>
 */
public interface CStaticIterableExtension<E> extends CIterableWaiter<E>, CIterableVerifier<E>, CIterableState<E> {
    @Override
    default boolean _useWaiter() {
        return false;
    }
}
