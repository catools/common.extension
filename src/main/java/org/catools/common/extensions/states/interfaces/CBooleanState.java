package org.catools.common.extensions.states.interfaces;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Objects;

/**
 * <p>CBooleanState is an interface for Boolean state related methods.</p>
 * <p>We need this interface to have possibility of adding state to any exists objects with the minimum change in the code.</p>
 * <p>It is critical to remember that we have 3 values for Boolean, True, False and Null</p>
 */
public interface CBooleanState extends CObjectState<Boolean> {

    default boolean isEqual(final Boolean expected) {
        return Objects.equals(get(), expected);
    }

    /**
     * Check if actual value is false
     *
     * @return execution boolean result
     */
    @JsonIgnore
    default Boolean isFalse() {
        return !get();
    }

    /**
     * Check if actual value is true
     *
     * @return execution boolean result
     */
    @JsonIgnore
    default Boolean isTrue() {
        return get();
    }
}
