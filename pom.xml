<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>org.catools</groupId>
    <artifactId>common.extension</artifactId>
    <version>0.1.39</version>
    <packaging>jar</packaging>

    <scm>
        <tag>HEAD</tag>
        <url>scm:git:https://gitlab.com/catools/common.extension.git</url>
    </scm>

    <name>Core Automation Common - Extensions</name>
    <description>The common extensions to improve code quality cross CATools projects and test</description>
    <url>https://gitlab.com/catools/common.extension.git</url>

    <developers>
        <developer>
            <name>Alireza Keshmiri</name>
            <email>kimiak2000@gmail.com</email>
            <url>https://www.linkedin.com/in/akeshmiri</url>
        </developer>
    </developers>

    <licenses>
        <license>
            <name>MIT License</name>
            <url>http://www.opensource.org/licenses/mit-license.php</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>11</maven.compiler.source>
        <maven.compiler.target>11</maven.compiler.target>

        <!-- Versions -->
        <catools.version>0.1.39</catools.version>
        <com.fasterxml.jackson.version>2.10.2</com.fasterxml.jackson.version>
        <slf4j.version>1.7.25</slf4j.version>
        <log4j.version>2.12.1</log4j.version>
        <lombok.version>1.18.10</lombok.version>

        <jmockit.version>1.44</jmockit.version>
        <!-- /Versions -->

        <!-- Repository Path -->
        <mvn.repo.url>https://repo.maven.apache.org/maven2</mvn.repo.url>
        <mvn.spring.repo.url>https://repo.spring.io/plugins-release/</mvn.spring.repo.url>
        <repo.snapshot.url>https://oss.sonatype.org/content/repositories/snapshots</repo.snapshot.url>
        <repo.release.url>https://oss.sonatype.org/service/local/staging/deploy/maven2</repo.release.url>
    </properties>
    <repositories>
        <repository>
            <id>maven</id>
            <url>${mvn.repo.url}</url>
        </repository>
        <repository>
            <id>snapshot</id>
            <url>${repo.snapshot.url}</url>
            <snapshots>
                <enabled>true</enabled>
                <updatePolicy>always</updatePolicy>
            </snapshots>
        </repository>
        <repository>
            <id>spring</id>
            <url>${mvn.spring.repo.url}</url>
        </repository>
    </repositories>
    <distributionManagement>
        <snapshotRepository>
            <id>snapshot</id>
            <url>${repo.snapshot.url}</url>
        </snapshotRepository>
        <repository>
            <id>central</id>
            <url>${repo.release.url}</url>
        </repository>
    </distributionManagement>
    <dependencies>
        <!-- ###################################################### -->
        <dependency>
            <groupId>org.testng</groupId>
            <artifactId>testng</artifactId>
            <version>7.1.0</version>
        </dependency>
        <dependency>
            <groupId>org.assertj</groupId>
            <artifactId>assertj-core</artifactId>
            <version>3.16.1</version>
        </dependency>
        <!-- ###################################################### -->
        <!-- ######################  CATS ######################### -->
        <dependency>
            <groupId>org.catools</groupId>
            <artifactId>common.utils</artifactId>
            <version>${catools.version}</version>
        </dependency>
        <!-- ######################  /CATS ######################### -->
        <!-- ###################### Apache ######################### -->
        <!-- apache -->
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-text</artifactId>
            <version>1.6</version>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
            <version>3.8.1</version>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-csv</artifactId>
            <version>1.6</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/org.apache.commons/commons-collections4 -->
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-collections4</artifactId>
            <version>4.4</version>
        </dependency>
        <!-- /apache -->
        <!-- ###################### /Apache ######################### -->
        <!-- ######################  Lombok ######################### -->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>${lombok.version}</version>
        </dependency>
        <!-- ######################  /Lombok ######################### -->
        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
            <version>2.6</version>
        </dependency>
        <dependency>
            <groupId>commons-collections</groupId>
            <artifactId>commons-collections</artifactId>
            <version>3.2.1</version>
        </dependency>
        <dependency>
            <groupId>com.googlecode.lambdaj</groupId>
            <artifactId>lambdaj</artifactId>
            <version>2.3.3</version>
        </dependency>
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
            <version>27.0-jre</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/org.bitbucket.cowwoc/diff-match-patch -->
        <dependency>
            <groupId>org.bitbucket.cowwoc</groupId>
            <artifactId>diff-match-patch</artifactId>
            <version>1.2</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/com.googlecode.libphonenumber/libphonenumber -->
        <dependency>
            <groupId>com.googlecode.libphonenumber</groupId>
            <artifactId>libphonenumber</artifactId>
            <version>8.10.7</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/uk.com.robust-it/cloning -->
        <dependency>
            <groupId>uk.com.robust-it</groupId>
            <artifactId>cloning</artifactId>
            <version>1.9.2</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/org.jmockit/jmockit -->
        <dependency>
            <groupId>org.jmockit</groupId>
            <artifactId>jmockit</artifactId>
            <version>${jmockit.version}</version>
        </dependency>
    </dependencies>
    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <version>3.0.1</version>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>3.0.1</version>
                <configuration>
                    <additionalOptions>
                        <additionalOption>-Xdoclint:none</additionalOption>
                    </additionalOptions>
                </configuration>
                <executions>
                    <execution>
                        <id>attach-javadocs</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-gpg-plugin</artifactId>
                <version>1.6</version>
                <executions>
                    <execution>
                        <id>sign-artifacts</id>
                        <phase>verify</phase>
                        <goals>
                            <goal>sign</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>3.8.0</version>
                    <configuration>
                        <source>${maven.compiler.source}</source>
                        <target>${maven.compiler.source}</target>
                        <release>${maven.compiler.source}</release>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-resources-plugin</artifactId>
                    <version>2.6</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <version>3.0.0-M4</version>
                    <configuration>
                        <argLine>
                            -Djava.net.preferIPv4Stack=true
                            -Duser.timezone=America/New_York
                            -Xmx4096m
                            -javaagent:${settings.localRepository}/org/jmockit/jmockit/${jmockit.version}/jmockit-${jmockit.version}.jar
                        </argLine>
                        <shutdown>kill</shutdown>
                        <useSystemClassLoader>false</useSystemClassLoader>
                        <reuseForks>false</reuseForks>
                        <workingDirectory>.</workingDirectory>
                        <threadCount>100</threadCount>
                        <parallel>methods</parallel>
                        <properties>
                            <property>
                                <name>junit</name>
                                <value>false</value>
                            </property>
                        </properties>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>
    <profiles>
        <profile>
            <id>release-sign-artifacts</id>
            <activation>
                <property>
                    <name>release</name>
                    <value>true</value>
                </property>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-javadoc-plugin</artifactId>
                        <version>3.0.1</version>
                        <executions>
                            <execution>
                                <id>attach-javadocs</id>
                                <goals>
                                    <goal>jar</goal>
                                </goals>
                            </execution>
                        </executions>
                        <configuration>
                            <additionalOptions>
                                <additionalOption>-Xdoclint:none</additionalOption>
                            </additionalOptions>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
</project>
