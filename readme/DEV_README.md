# C.A.T.s Development Guide 

- Versions:

    - Java 11 (https://download.java.net/java/GA/jdk11/9/GPL/openjdk-11.0.2_windows-x64_bin.zip)
    - Maven 3.6.3 or higher

- Make sure you have java 11 in your JAVA_HOME if you want to use command line to compile project

- put following line in your testNG Run/Debug configuration for VM parameter (we need this for JMockit)

- Add Lombok and Enable Annotation Processing for your idea
```
    -ea -javaagent:/root/.m2/repository/org/jmockit/jmockit/1.44/jmockit-1.44.jar
```

- in case if you want to execute maven using command line use the following format:

```
    mvn clean package
```

Please note that SureFire has some issues with fork and son windows 10 (at least for me) and it seems that the
problem is related to handling parallel logging in console. if you have same problem and you get VM crashed error then
try to redirect log to file.
It is not only speeding up the test execution for ove 10 times but help with crash sa well.

```
    mvn clean package > ./test-output/log.txt
```

#GPG
Follow [gpg_signed_commits](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/)


##Send Key To Server
```
gpg2 --send-keys --keyserver hkps://keyserver.ubuntu.com XXXXXX
```

##Receive Key To Server
```
gpg2 --keyserver hkps://keyserver.ubuntu.com --receive-key XXXXXX
```

##Export Keys
```
gpg --output pubkey.gpg --armor --export XXXXXX
gpg --output seckey.gpg --armor --export-secret-key XXXXXX
```

##Import Keys
```
gpg --import pub
gpg --allow-secret-key-import --import key

```
